#!/usr/bin/env python3

# Gmail Filter Label: "from:(sender@domain.com) subject:filteredSubject"
# Limit: 100 email get request at once
# Options: "--silent": no print on STDOUT; "--nolog": no log files; "--email": email report in case of failed tests; "--help"
# Gmail API: https://developers.google.com/gmail/api/quickstart/python
# Pytest: https://github.com/reportportal/agent-python-pytest

from __future__ import print_function
from pathlib import Path
from datetime import datetime
from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
from email.mime.text import MIMEText
import pickle
import base64
import os.path
import sys
import argparse
import re
import platform


my_os = platform.system()
if my_os == 'Windows':
    slash = '\\'
else:
    slash = '/'

# Initiate the parser
parser = argparse.ArgumentParser()
parser.add_argument(
    "-s", "--silent", help="no data displayed on STDOUT", action="store_true")
parser.add_argument("-n", "--nolog", help="no log files", action="store_true")
parser.add_argument(
    "-e", "--email", help="send warning emails to the given address")
# Read arguments from the command line
args = parser.parse_args()

# If modifying these scopes, delete the file: token.pickle.
SCOPES = ['https://www.googleapis.com/auth/gmail.modify']


def create_message(sender, to, subject, message_text):
    message = MIMEText(message_text)
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject
    raw_message = base64.urlsafe_b64encode(message.as_string().encode("utf-8"))
    return {
        'raw': raw_message.decode("utf-8")
    }


def send_message(service, user_id, message):
    try:
        message = service.users().messages().send(
            userId=user_id, body=message).execute()
        print('Message Id: %s' % message['id'] + '\n')
        return message
    except Exception as e:
        print('An error occurred: %s' % e)
        return None


def log(date, template, id, env):
    duplication = False
    current_folder = str(os.path.dirname(os.path.abspath(__file__)))
    # Create folder for the logs if it doesn't exist
    if not os.path.exists(current_folder + slash + 'logs'):
        os.makedirs(current_folder + slash + 'logs')

    if env == 'env1':
        path = str(Path('logs').resolve()) + slash + 'log_env1.txt'
    elif env == 'env2':
        path = str(Path('logs').resolve()) + slash + 'log_env2.txt'
    elif env == 'env3':
        path = str(Path('logs').resolve()) + slash + 'log_env3.txt'
    elif env == 'env4':
        path = str(Path('logs').resolve()) + slash + 'log_env4.txt'
    elif env == 'env5':
        path = str(Path('logs').resolve()) + slash + 'log_env5.txt'
    else:
        print(
            '\n\033[31m', 'ERROR: Invalid environment name for logging.', '\033[0m\n')

    date_str = str(date)
    template_str = str(template)
    id_str = str(id)

    # Create the log file if it doesn't exist and open it in append mode
    # print(path)
    if not os.path.exists(path):
        open(path, 'w').close()

    with open(path, 'r+') as file:
        for line in file:
            if date_str in line:
                duplication = True
        if duplication == False:
            with open(path, 'a') as fh:
                fh.write(date_str + '; ' + template_str + '; ' + id_str + '\n')


def parse_msg(msg):
    if msg.get("payload").get("body").get("data"):
        return base64.urlsafe_b64decode(msg.get("payload").get("body").get("data").encode("ASCII")).decode("utf-8")
    return msg.get("snippet")


def email_type(txt):
    if "template_1" in txt:
        return "email_type_1"
    elif "template_2" in txt:
        return "email_type_2"
    elif "template_3" in txt:
        return "email_type_3"
    else:
        return "Unknown email type"


def main():
    """Gives back the unreaded emails under the "customFilter" label."""
    send_email = False  # Enable it with the --email EMAIL option
    creds = None
    # Check if the silent mode is enabled
    if args.silent:
        sys.stdout = open(os.devnull, 'w')

    if args.email is not None:
        email_address = args.email
        regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'
        if(re.search(regex, email_address)):
            send_email = True
        else:
            print("Invalid Email Address")

    # The file token.pickle stores the user's access and refresh tokens, and is
    # created automatically when the authorization flow completes for the first
    # time.
    if os.path.exists(os.path.abspath('token.pickle')):
        with open(os.path.abspath('token.pickle'), 'rb') as token:
            creds = pickle.load(token)

    # If there are no (valid) credentials available, let the user log in.
    if not creds or not creds.valid:
        if creds and creds.expired and creds.refresh_token:
            creds.refresh(Request())
        else:
            flow = InstalledAppFlow.from_client_secrets_file(
                os.path.abspath('credentials.json'), SCOPES)
            creds = flow.run_local_server(port=0)

        # Save the credentials for the next run
        with open(os.path.abspath('token.pickle'), 'wb') as token:
            pickle.dump(creds, token)

    service = build('gmail', 'v1', credentials=creds)

    # Call the Gmail API
    results = service.users().messages().list(
        userId='me', q='in:customFilter is:unread').execute()
    unread_msgs = results.get('messages', [])

    if not unread_msgs:
        print('\nYou have no new alert email.\n')
    else:
        message_count = 0
        env1_counter = 0
        env2_counter = 0
        env3_counter = 0
        env4_counter = 0
        env5_counter = 0
        env1_date_summary = []
        env2_date_summary = []
        env3_date_summary = []
        env4_date_summary = []
        env5_date_summary = []
        environments_with_no_mail = []
        # Don't change the order of the elements in the following variable
        environment_list = ["env1", "env2", "env3", "env4", "env5"]

        print('\n\033[36m', 'Verified:', datetime.now().astimezone().strftime(
            "%Y-%m-%dT%H:%M:%S %z"), '\033[0m\n')
        for message in unread_msgs:
            message_count += 1
            msg = service.users().messages().get(
                userId='me', id=message['id'], format='full').execute()
            message_text = parse_msg(msg)
            message_id = msg.get('id')
            email_template = email_type(message_text)
            message_header = msg.get('payload').get('headers')
            for item in message_header:
                if item['name'] == 'Date':
                    message_date = item['value']
            print('Message: ' + str(message_id))

            # Mark the opened email as READ
            service.users().messages().modify(userId='me', id=message['id'], body={
                'removeLabelIds': ['UNREAD']}).execute()

            if "env1_unique_text" in message_text:
                env1_counter += 1
                env1_date_summary.append(str(message_date))
                if args.nolog:
                    continue
                else:
                    log(message_date, email_template, message_id, 'env1')
            elif "env2_unique_text" in message_text:
                env2_counter += 1
                env2_date_summary.append(str(message_date))
                if args.nolog:
                    continue
                else:
                    log(message_date, email_template, message_id, 'env2')
            elif "env3_unique_text" in message_text:
                env3_counter += 1
                env3_date_summary.append(str(message_date))
                if args.nolog:
                    continue
                else:
                    log(message_date, email_template, message_id, 'env3')
            elif "env4_unique_text" in message_text:
                env4_counter += 1
                env4_date_summary.append(str(message_date))
                if args.nolog:
                    continue
                else:
                    log(message_date, email_template, message_id, 'env4')
            elif "env5_unique_text" in message_text:
                env5_counter += 1
                env5_date_summary.append(str(message_date))
                if args.nolog:
                    continue
                else:
                    log(message_date, email_template, message_id, 'env5')
            else:
                print(
                    '\n\033[31m', 'ERROR: The source environment is unknown.', '\033[0m\n')

        if (env1_counter + env2_counter + env3_counter + env4_counter + env5_counter) >= 1:
            print('\nYou have ' + str(message_count) +
                  ' unread email in total:\n')
            print(str(env1_counter) + ' email from env1.' +
                  ' Recieved: ' + '; '.join(env1_date_summary))
            print(str(env2_counter) + ' email from env2.' +
                  ' Recieved: ' + '; '.join(env2_date_summary))
            print(str(env3_counter) + ' email from env3.' +
                  ' Recieved: ' + '; '.join(env3_date_summary))
            print(str(env4_counter) + ' email from env4.' +
                  ' Recieved: ' + '; '.join(env4_date_summary))
            print(str(env5_counter) + ' email from env5.' +
                  ' Recieved: ' + '; '.join(env5_date_summary))
            print('\nThe verified emails have been marked as read.\n')
        else:
            print('Unknown error during email processing.')

        # Don't change the order of the elements in the following variable
        environment_counters = [
            env1_counter, env2_counter, env3_counter, env4_counter, env5_counter]

        if 0 in environment_counters and send_email:
            index_environments_with_no_email = [
                i for i, x in enumerate(environment_counters) if x == 0]

            for x in index_environments_with_no_email:
                environments_with_no_mail.append(environment_list[x])

            email_message = create_message('reportSender@domain.com', str(args.email), 'WARNING', 'Verified: ' + str(
                datetime.now().astimezone().strftime("%Y-%m-%dT%H:%M:%S %z")) + '\n' + 'No email from: ' + str(environments_with_no_mail))
            send_message(service, 'me', email_message)


if __name__ == '__main__':
    main()
