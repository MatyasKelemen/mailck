#!/usr/bin/env python3

# Report Portal: https://github.com/reportportal/agent-python-pytest

import logging
import time
import platform
import os.path
import pytest
import datetime
from pathlib import Path
from reportportal_client import RPLogger
from dateutil.parser import parse

my_os = platform.system()
if my_os == 'Windows':
    slash = '\\'
else:
    slash = '/'

env1_log_file = str(Path('logs').resolve()) + slash + 'log_env1.txt'
env2_log_file = str(Path('logs').resolve()) + slash + 'log_env2.txt'
env3_log_file = str(Path('logs').resolve()) + slash + 'log_env3.txt'
env4_log_file = str(Path('logs').resolve()) + slash + 'log_env4.txt'
env5_log_file = str(Path('logs').resolve()) + slash + 'log_env5.txt'


@pytest.fixture(scope="session")
def rp_logger():
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)
    logging.setLoggerClass(RPLogger)
    return logger


def is_date(string, fuzzy=False):
    """:param fuzzy: bool, ignore unknown tokens in string if True"""
    string = string.split(';', 1)[0] # Get everything before the first semicolon
    print(string)
    try: 
        parse(string, fuzzy=fuzzy)
        return True
    except ValueError:
        return False

def is_today(string, fuzzy=False):
    """:param fuzzy: bool, ignore unknown tokens in string if True"""
    string = string.split(',', 1)[0] # Get everything before the first colon
    print(string)
    today = datetime.datetime.now()
    try: 
        parse(string, fuzzy=fuzzy)
        if string == today.strftime("%a"):
            return True
    except ValueError:
        return False


def template_sum(date_sum):
    string = str(date_sum)
    type_1_email_sum = string.count("email_type_1")
    type_2_email_sum = string.count("email_type_2")
    type_3_email_sum = string.count("email_type_3")
    return "Type_1 email count: " + str(document_email_sum) + " Type_2 email count: " + str(search_email_sum) + " Type_3 email count: " + str(research_email_sum)

@pytest.mark.parametrize("log_file", [env1_log_file, env2_log_file, env3_log_file, env4_log_file, env5_log_file])
def test_env(rp_logger, log_file):
    if os.path.exists(log_file):
        valid_dates = []
        invalid_dates = []
        with open(log_file, 'r') as file:
            for line in file:
                if is_date(line) and is_today(line):
                    valid_dates.append(line.rstrip("\n"))
                else:
                    invalid_dates.append(line.rstrip("\n"))
        assert (os.path.getsize(log_file) > 32) == True
        rp_logger.info("Emails: " + str(valid_dates))
        time.sleep(1)
        rp_logger.debug("Emails with invalid date: " + str(invalid_dates))
        time.sleep(1)
        rp_logger.info(template_sum(valid_dates))
    else:
        rp_logger.info("No log file.")
        assert False
