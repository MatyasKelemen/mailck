python3 -m pip install -r requirements.ini

python3 -m pytest --setup-only -c pytest.ini

python3 ./cma.py --email reportReceiver@domain.com

python3 -m pytest --junitxml results.xml --reportportal
